
## Github repo for [Proper Cursive](http://propercursive.com) website.
 

Built with my "Very Simple Node" starter app. [See Github Repo](https://github.com/propercursive/node-express-html5boilerplate)


### Licenses

-------

Node: See https://github.com/joyent/node

-------

Express: MIT license https://github.com/visionmedia/express

-------

HTML5 Boilerplate https://github.com/h5bp/html5-boilerplate

jQuery: MIT/GPL license

Modernizr: MIT/BSD license

Respond.js: MIT/GPL license

Normalize.css: Public Domain

-------

Initializr 2: https://github.com/verekia/initializr
